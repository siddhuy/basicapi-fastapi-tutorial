# Basic Rest API with FastAPI (Tutorial)

This repo is created for educational purposes. Keep that in mind before you use it in production.

## Setup Instructions

Follow these steps to run the demo api and explore its endpoints.

1. Create Virtual Environment
    `$ virtualenv venv`
2. Activate the Virtual Environment
    `$ source venv/bin/activate`
3. Install the project requirements
    `$ pip install -r requirements.txt`
4. You also need postgres database running on `http://localhost:5432` with password **'admin123456'** 
    `$ docker run --name pgdb -p 5432:5432 --cpus=0.5 -m 256M -e POSTGRES_PASSWORD=admin123456 -d postgres`
5. Then change directory into src
    `$ cd src`
6. Finally, start the REST app with
    `$ uvicorn main:app`

Go to `http://localhost:8000/docs` and check out the docs.

## Devoloper's Note

This project doesnt work properly on Windows due to the heavy usage of async/await syntax and also due to uvloop. Try it in Linux or WSL. 