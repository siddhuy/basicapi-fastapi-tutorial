from fastapi import FastAPI
import users.routes as user_routes
from db import initialize_db, dispose_db
import asyncio
from tags import Tags

app = FastAPI()

app.include_router(user_routes.router, prefix="/users", tags=[Tags.USERS])


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.on_event("startup")
async def startup():
    await initialize_db()


@app.on_event("shutdown")
async def shutdown():
    await dispose_db()