from fastapi import APIRouter, Response
from users.models import (  UserSignupModel, User, UserValidationError,
                            UserNameLogin, UserEmailLogin
                            )
from db  import DBSession
from sqlalchemy import select, or_
from users.utils import is_user_eligible_for_signup, login_with_email, login_with_username
from typing import Union
from fastapi.exceptions import HTTPException


router = APIRouter()


@router.post("/signup")
async def signup(user: UserSignupModel):
    async with DBSession() as session:
        async with session.begin():
            if await is_user_eligible_for_signup(user, session):
                try:
                    db_user = User(username = user.username, email_id= user.email_id)
                    db_user.gen_hash(user.password)
                    del user            
                    session.add(db_user)
                    await session.commit()
                    return { "message": "user added successfully!" }
                except UserValidationError as err:
                    return {
                        "message": f"user validation failed due to {err}"
                    }
                    

@router.post("/login")
async def login(user: Union[UserNameLogin, UserEmailLogin], response: Response):
    async with DBSession() as session:
        async with session.begin():
            if isinstance(user, UserNameLogin):
                session_token = await login_with_username(user)
                if session_token:
                    response.set_cookie(key="session_token", value=session_token)
                    return {
                        "message": "Login Successfull!"
                    }
            elif isinstance(user, UserEmailLogin):
                session_token = await login_with_email(user)
                if session_token:
                    response.set_cookie(key="session_token", value=session_token)
                    return {
                        "message": "Login Successfull!"
                    }
            else:
                return {
                    "message": "Login Failed."
                }