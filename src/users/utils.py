from sqlalchemy import select, or_
from users.models import ( UserNameLogin, UserSignupModel, User, UserEmailLogin)
from db import DBSession
from typing import Dict, Optional, Union, Tuple
from fastapi.exceptions import HTTPException


async def is_user_eligible_for_signup(user: UserSignupModel, session: DBSession) -> Tuple[bool, Optional[str]]:
    """
    Checks if the user is eligible for signup, by making sure there are no users
    in the database with same username and email id.

    Args:
        user (UserSignupModel): needs username, email id and password fields for
            signing up.
        session (DBSession): pass sqlalchemy scoped session after calling its begin
            method

    Returns:
        Union[Tuple[bool, str], Tuple[bool, None]]: returns False and Why the validation failed or
            return True if the user is eligible
    """
    sql_statement = select(User).where(
        or_(User.username==user.username,
            User.email_id==user.email_id)
        )
    results = await session.stream_scalars(sql_statement)
    existing_user = await results.first()
    if existing_user:
        errors = []
        if user.username == existing_user.username:
            errors.append(f"User with {user.username} already exists")
        if user.email_id == existing_user.email_id:
            errors.append(f"User with {user.email_id} already exists!")
        raise HTTPException(status_code=400, detail={ 
                                                     "message": "User is not eligible for signup",
                                                     "reason": errors
                                                     })
    else:
        return True


async def get_user_from_db(
        user: Union[UserNameLogin, UserEmailLogin, UserSignupModel], 
        session: DBSession) -> Optional[User]:
    """
    Queries the database and finds the user if exists or return 'None'

    Args:
        user (Union[UserNameLogin, UserEmailLogin, UserSignupModel]): _description_
        session (DBSession): _description_

    Returns:
        Optional[User]: _description_
    """
    
    
    pass


async def login_with_username(user: UserNameLogin):
    pass


async def login_with_email(user: UserEmailLogin):
    pass




