from ipaddress import ip_address
from sqlalchemy import Column, Integer, String, DateTime
from datetime import datetime
from pydantic import BaseModel
from sqlalchemy.orm import validates
import re
from passlib.hash import argon2
import secrets
import db
from typing import List
from sqlalchemy.sql import func


class UserSignupModel(BaseModel):
    """
    For signing up user.

    Args:
        first_name (str) :
        last_name  (str) :
        username   (str) :
        password   (str) :
        email_id   (str) :
    """
    first_name: str
    last_name: str
    username: str
    password: str
    email_id: str


class UserNameLogin(BaseModel):
    """
    For Logging In with username

    Args:
        username (str) :
        password (str) :
    """
    username:str
    password:str


class UserEmailLogin(BaseModel):
    """
    For logging in with Email Id.

    Args:
        BaseModel (_type_): _description_
    """
    email_id: str
    password: str


class UserValidationError(Exception):
    def __init__(self, key, message) -> None:
        self.key = key
        self.message = message
        super().__init__(self.message)
    
    def __str__(self) -> str:
        return f"Error occured while validating {self.key} {self.message}"


class User(db.Base):
    __tablename__ = "users"
    id: int = Column("id", Integer, primary_key=True)
    username: str = Column("username", String(64), unique=True, nullable=False, index=True)
    password_hash: str = Column("password_hash", String(128), nullable=False)
    email_id: str = Column("email_id", String(320), unique=True, nullable=False, index=True)
    token_id: str = Column("token_id", String(120), unique=True, index=True)
    registration_time: datetime = Column("registration_time", DateTime(timezone=True), default=func.now())
    
    USERNAME_RE = re.compile(r"\A[A-Z0-9_.]{5,64}\Z", re.IGNORECASE)
    EMAIL_RE = re.compile(r"([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|\[[\t -Z^-~]*])")

    TOKEN_BYTES = 90 # 120 characters long


    def dict(self):
        return {
            "username": self.username,
            "email": self.email
        }

    def gen_hash(self, password: str):
        self.password_hash = argon2.hash(password)

    def verify(self, password: str):
       return argon2.verify(password, self.password_hash)

    @validates("username")
    def validate_username(self, key, username):
        if User.USERNAME_RE.fullmatch(username):
            return username
        else:
            raise UserValidationError(key, ("because it can only contain alphabets, numbers," 
            f" dots and underscores and must be between 5-64 characters long but recieved {username}"))
    
    @validates("email_id")
    def validate_email_id(self, key, email_id: str):
        if User.EMAIL_RE.fullmatch(email_id):
            return email_id
        else:
            raise UserValidationError(key, f"because {email_id} is not valid")
    
    def gen_token(self):
        self.token_id =  secrets.token_urlsafe(User.TOKEN_BYTES)



class LoggedInDevice(db.Base):
    __tablename__="loggedindevices"
    id: int = Column("id", Integer, primary_key=True)
    ip_address = Column("ip_address", String(45))
    

class UserSession(db.Base):
    __tablename__="usersessions"
    id: int = Column("id", Integer, primary_key=True)
    token: str = Column("token", String(120), unique=True, index=True)
    login_time: datetime = Column("login_time", DateTime(), server_default=func.now())
    # device: List[LoggedInDevice] = 
    
    TOKEN_BYTES = 90 # 120 characters long

