from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from asyncio import current_task
from sqlalchemy.ext.asyncio import async_scoped_session
from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine

Base = declarative_base()

engine: AsyncEngine = create_async_engine(
    "postgresql+asyncpg://postgres:admin123456@localhost:5432/testdb", echo=False,
)

async_session_factory = sessionmaker(
    engine, expire_on_commit=False, class_=AsyncSession
)

DBSession = async_scoped_session(
    async_session_factory, scopefunc=current_task
)


async def initialize_db():
    async with engine.begin() as connection:
        # await connection.run_sync(Base.metadata.drop_all)
        await connection.run_sync(Base.metadata.create_all)

async def dispose_db():
    await engine.dispose()